===============================
networking-edge-vpn
===============================

API definition and the implementation of an Edge VPN service in OpenStack

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/networking-edge-vpn
* Source: http://git.openstack.org/cgit/openstack/networking-edge-vpn
* Bugs: http://bugs.launchpad.net/edge-vpn

Features
--------

* TODO
