============
Installation
============

At the command line::

    $ pip install networking-edge-vpn

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv networking-edge-vpn
    $ pip install networking-edge-vpn
